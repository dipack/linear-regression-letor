# CSE 574 - LeToR problem
Solving the Learning-To-Rank (LeToR) problem using Linear Regression.

The dataset is 'Querylevelnorm.txt' from the MQ2007 dataset, provided by [Microsoft|http://research.microsoft.com/en-us/um/beijing/projects/letor/letor4dataset.aspx].
